from rest_framework import serializers
from .models import Student


# Validators
def starts_with_r(value):
    if value[0].lower() != 'r':
        raise serializers.ValidationError("Name must start with R")


class StudentSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, validators=[starts_with_r])
    roll = serializers.IntegerField()
    city = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return Student.objects.create(**validated_data)

    def update(self, instance, validated_data):  # instance means old data dict, validated_data is new data dict
        instance.name = validated_data.get('name', instance.name)
        instance.roll = validated_data.get('roll', instance.roll)
        instance.city = validated_data.get('city', instance.city)
        instance.save()
        return instance

    # Field level Validation (used for single validation)
    def validate_roll(self, value):
        if value >= 200:
            raise serializers.ValidationError("Seats are full")  # Raising the error
        return value

    # Object level Validation (used for multiple validations)
    def validate(self, data):
        name = data.get('name')
        city = data.get('city')

        if name.lower() == "sachnaam bedi" and city.lower() == "ahmedabad":
            raise serializers.ValidationError("City must be Amritsar")
        return data
